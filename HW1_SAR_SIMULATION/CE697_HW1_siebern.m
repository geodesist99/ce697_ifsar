%% CE 697 INSAR HW#1
% Submitted by Chris Siebern
%
% Due 1/26/2015

%% Problem Statement
% Simulate several SAR images using the end to end SAR equation:
%
% $$y_{out,n,m(\xi_T,\tau_T)}=
% \alpha e^{-j\frac{4\pi}{\lambda}R_T}
% sinc[B_x(mx_s-\xi_T)]
% sinc[(B(n\tau_s-\tau_T)]$$
%
% with the following parameters:
%
% * Image Size:  500m x 200m (Range x Azimuth) 
% * Azimuth Resolution:      5m
% * Range Resolution:       10m
%
% Create the following images:
%
% * A single target with gaussian noise
% * Two separated targets with gaussian noise
% * Two distributed targets with gaussian noise

clc
clear
close all
figWidth = 1120; % pixels
figHeight = 840;
rect = [0 50 figWidth figHeight];

%% Define SAR Bandwidths and Carrier Frequency
% Given Sampling Parameters:
%
% * $x_s$ = 5 meters
% * $\tau_s$ = 10 meters
%
% The SAR sensor parameters are undefined in the problem statement.  I have
% defined the range and azimuth bandwidths of the SAR system using the
% requested resolutions and the Nyquist-Shannon Sampling Theorem.
% (<https://en.wikipedia.org/wiki/Nyquist?Shannon_sampling_theorem>)
%
% * $B=\frac{1}{2\tau_s}$
% * $B_x=\frac{1}{2x_s}$
%
% I was free to choose a carrier frequency, so I used the frequency of the
% TerraSAR-X sensor
% (<http://www.geo-airbusds.com/en/228-terrasar-x-technical-documents>)
%
% * Radar Carrier Frequency = 9.65 GHz
% * $\lambda = \frac{1}{9.65e9}$ meters
xs = 5;
ts = 10;

B  = 1/(2*ts);
Bx = 1/(2*xs);

lambda = 1/9.65e9;

%% Prepare for SAR Image Arrays
% The problem requires that the simulated images be 500 meters in range by
% 200 meters in azimuth.  Using the sampling intervals provided, the image
% dimensions in pixels are found by:
%
% * $\frac{500}{x_s} = 50$ pixels
% * $\frac{200}{\tau_s} = 40$ pixels
%
% I used the meshgrid() function in matlab to prepare the 2d integer arrays
% mgrid and ngrid needed to calculate the cardinal sine functions.
m = (1:5:200)/xs;
n = (1:10:500)/ts;
[mgrid,ngrid] = meshgrid(m,n);


%% Define Target 1
% I am free to select the coordinates (Azimuth and range) of the target,
% the range to the target, and its attenuation.  I assigned the following
% values:
%
% * $\xi_T$ = 50 meters
% * $\tau_T$ = 200 meters
% * $R_{T1}$ = 500 meters
% * $\alpha$ = 0.4
xi_t1 = 50;         % Azimuth Value in Meters
tau_t1 = 200;       % Range Coordinate in Meters
R_t1 = 500;           % Target Distance in Meters
alpha_t1 = 0.4;     % Signal Attenuation

%% Azimuth and Range Profiles
% The following plots show cardinal sine component profiles for
% azimuth and range of Target 1
%

figure('OuterPosition', rect); 
plot(mgrid(50/xs,:),sinc(Bx*(mgrid(50/xs,:)*xs - xi_t1)),'o-')
title('Azimuth Signal"Target 1"')
xlabel('Azimuth (pixels)')
ylabel('Intensity')
grid on

figure('OuterPosition', rect); 
plot(ngrid(:,200/ts),sinc(B*(ngrid(:,200/ts)*ts - tau_t1)),'o-')
title('Range Signal "Target 1"')
xlabel('Range (pixels)')
ylabel('Intensity')
grid on
%% Simulate the end to end SAR Image for Target T1
% To simulate the images I construct the image by multiplying the
% attenuation and phase scalars by the element by element product of the
% azimuth and range cardinal sines.  Finally I add a gaussian noise
% component to the result.
%
% * $y_{noise} = 0.01e^{-j(randn_{50x40})}$
%
% The results are shown in the SLC intensity and phase plots.
ym = sinc(Bx*(mgrid*xs - xi_t1));
yn = sinc(B*(ngrid*ts - tau_t1));
rho_t1 = exp(-1j*4*pi/lambda*R_t1);
noise_t1 = 0.01*exp(-1j*randn(50,40));
y_t1 = alpha_t1*rho_t1*ym.*yn + noise_t1;

figure('OuterPosition', rect); imagesc(abs(y_t1)); colorbar
title('Simulated SAR Intensity Image "Target 1"')
xlabel('Azimuth (pixels)')
ylabel('Range (pixels)')

figure('OuterPosition', rect); imagesc(angle(y_t1)); colorbar
title('Simulated SAR Phase Image "Target 1"')
xlabel('Azimuth (pixels)')
ylabel('Range (pixels)')

%%
% I have a question about how we are simulating the noise.  In my
% simulation, I've simply generated a random complex array, applied a
% constant attenuation value, and then added it to the pure simulated
% signal.  Is this a valid model for simulating signal from the
% environment, or is this just a simplification for the exercise?  I'm
% curious about system noise sources for these EM signals we are working
% with.
%
% As an example, in GPS we learned about how the signal is both delayed and
% accelerated depending on ionosphere and troposphere effects.  It seems
% this would affect the phase angles as well.
%
% Another example would be the efect of multipath and reflections on the
% phase angle.  If we were to look at a phase plot of the EM signal as it
% is reflected, is the phase angle continuous, or are there steps or shifts
% in the signal?  Does it depend on the conductive properties of the
% reflector?

%% Define Target 2
%
% * $\xi_T$ = 150 meters
% * $\tau_T$ = 50 meters
% * $R_{T1}$ = 525 meters
% * $\alpha$ = 0.6
xi_t2 = 150;         % Azimuth Value in Meters
tau_t2 = 50;       % Range Coordinate in Meters
R_t2 = 525;           % Target Distance in Meters
alpha_t2 = 0.6;     % Signal Attenuation

%% Simulate the end to end SAR Image for Target T2
%
ym = sinc(Bx*(mgrid*xs - xi_t2));
yn = sinc(B*(ngrid*ts - tau_t2));
rho_t2 = exp(-1j*4*pi/lambda*R_t2);
noise_t2 = 0.01*exp(-1j*randn(50,40));
y_t2 = alpha_t2*rho_t2*ym.*yn + noise_t2;

figure('OuterPosition', rect); imagesc(abs(y_t2)); colorbar
title('Simulated SAR Intensity Image "Target 2"')
xlabel('Azimuth (pixels)')
ylabel('Range (pixels)')

figure('OuterPosition', rect); imagesc(angle(y_t2)); colorbar
title('Simulated SAR Phase Image "Target 2"')
xlabel('Azimuth (pixels)')
ylabel('Range (pixels)')

%% Define Target 3
%
% * $\xi_T$ = 40 meters
% * $\tau_T$ = 210 meters
% * $R_{T1}$ = 524 meters
% * $\alpha$ = 0.5
xi_t3 = 40;         % Azimuth Value in Meters
tau_t3 = 210;       % Range Coordinate in Meters
R_t3 = 524;         % Target Distance in Meters
alpha_t3 = 0.5;     % Signal Attenuation

%% Simulate the end to end SAR Image for Target T3
%
ym = sinc(Bx*(mgrid*xs - xi_t3));
yn = sinc(B*(ngrid*ts - tau_t3));
rho_t3 = exp(-1j*4*pi/lambda*R_t3);
noise_t3 = 0.01*exp(-1j*randn(50,40));
y_t3 = alpha_t3*rho_t3*ym.*yn + noise_t3;

figure('OuterPosition', rect); imagesc(abs(y_t3)); colorbar
title('Simulated SAR Intensity Image "Target 3"')
xlabel('Azimuth (pixels)')
ylabel('Range (pixels)')

figure('OuterPosition', rect); imagesc(angle(y_t3)); colorbar
title('Simulated SAR Phase Image "Target 3"')
xlabel('Azimuth (pixels)')
ylabel('Range (pixels)')

%% Combine Target 1 and Target 2 with Noise
%
figure('OuterPosition', rect); imagesc(abs(y_t1 + y_t2 - noise_t2)); colorbar
title('Simulated SAR Intensity Image "Targets 1 and 2"')
xlabel('Azimuth (pixels)')
ylabel('Range (pixels)')

figure('OuterPosition', rect); imagesc(angle(y_t1 + y_t2 - noise_t2)); colorbar
title('Simulated SAR Phase Image "Targets 1 and 2"')
xlabel('Azimuth (pixels)')
ylabel('Range (pixels)')

%% Combine Target 1 and Target 3 with Noise
%
figure('OuterPosition', rect); imagesc(abs(y_t1 + y_t3 - noise_t3)); colorbar
title('Simulated SAR Intensity Image "Targets 1 and 3"')
xlabel('Azimuth (pixels)')
ylabel('Range (pixels)')

figure('OuterPosition', rect); imagesc(angle(y_t1 + y_t3 - noise_t3)); colorbar
title('Simulated SAR Phase Image "Targets 1 and 3"')
xlabel('Azimuth (pixels)')
ylabel('Range (pixels)')